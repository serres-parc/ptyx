import math


class Calc:

    def findAverage(self, measurements):
        average = "%.3f" % float(sum(measurements) / len(measurements))
        return float(average)

    def deviation(self, measurements):
        average = sum(measurements) / len(measurements)
        s = 0
        for i in measurements:
            s = s + (i - average) ** 2
        std = "%.3f" % float(math.sqrt(s / (len(measurements) - 1)))
        return float(std)
