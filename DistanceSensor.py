import RPi.GPIO as GPIO
import time


class DistanceSensor:

    def distance(self, TRIGGER, ECHO, TICKS, TEMPERATURE,triggerTime):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(TRIGGER, GPIO.OUT)
        GPIO.setup(ECHO, GPIO.IN)
        measurements = []
        speedOfSound = (331.3 + 0.606 * TEMPERATURE) * 100
        for i in range(TICKS):
            GPIO.output(TRIGGER, 1)
            time.sleep(triggerTime)
            GPIO.output(TRIGGER, 0)
            startTime = time.time()
            stopTime = time.time()
            while GPIO.input(ECHO) == 0:
                startTime = time.time()
            while GPIO.input(ECHO) == 1:
                stopTime = time.time()
            timeEl = stopTime - startTime
            distance = (timeEl * speedOfSound) / 2
            measurements.append(distance)
        averageDist = sum(measurements) / len(measurements)
        GPIO.cleanup()
        return averageDist

