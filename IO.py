import csv

class IO:

    def __init__(self, file):
        self.f = open(file, "w+")

    def writeToFile(self, file, data):
        f = open(file, "a+")
        f.write(data)
        f.close()
    def writeToCSVFile(self,file,data):
        with open(file,mode='a+') as f:
            f = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            f.writerow(data)

    def readRawFile(self, fileToOpen):
        file = open(fileToOpen, "r")
        data = []
        for x in file:
            data.append(x)
        return data

    def repeater(self, REPS):
        if REPS == 5:
            return 10
        elif REPS == 10:
            return 25
        elif REPS == 25:
            return 50
        elif REPS == 50:
            return 75
        elif REPS == 75:
            return 100
        else:
            return 5


    def changeTriggerTime(self, time):
        if time == 0.000010:
            return 0.000015
        elif time == 0.000015:
            return 0.000020
        else:
            return 0.000010

    def changeWaitTime(self, time):
        if time == 0.005:
            return 0.010
        elif time == 0.010:
            return 0.015
        elif time == 0.015:
            return 0.020
        else:
            return 0.005
