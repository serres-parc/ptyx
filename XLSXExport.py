import csv
from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
import statistics
from decimal import Decimal
from openpyxl.chart import LineChart, Reference, Series
import re


class XLXSExport:
    avalailableSensors = ["G1", "G2", "G3", "G4", "G5", "b2", "b3", "D", "N1", "N2", "S1", "S2", "S3", "S4", "S5", "P1", "P2"]
    availableAngles =["0","10L","20L","30L","45L","60L","10R","20R","30R","45R","60R"]
    availableCategories =["Grove","HC-SR04","HY-SRF05","Ping"]
    availableDistances =["0.5","1","2","3","4"]
    rowrange = {"0" : [2,12],"10L":[15,25],"20L":[28,38],"30L":[41,51],"45L":[54,64],"60L":[67,77],"10R":[15,25],"20R":[28,38],"30R":[41,51],"45R":[54,64],"60R":[67,77]}
    colrange = {"0" : [1,8],"10L":[1,8],"20L":[1,8],"30L":[1,8],"45L":[1,8],"60L":[1,8],"10R":[11, 18],"20R":[11, 18],"30R":[11, 18],"45R":[11, 18],"60R":[11, 18]}
    avgRow = {"0" :[7],"10L":[6],"20L":[5],"30L":[4],"45L":[3],"60L":[2],"10R":[8],"20R":[9],"30R":[10],"45R":[11],"60R":[12]}
    avgDevRow = {"0": [23], "10L": [22], "20L": [21], "30L": [20], "45L": [19], "60L": [18], "10R": [24], "20R": [25], "30R": [26],"45R": [27], "60R": [28]}
    absErrorRow = {"0": [39], "10L": [38], "20L": [37], "30L": [36], "45L": [35], "60L": [34], "10R": [40], "20R": [41],"30R": [42], "45R": [43], "60R": [44]}
    relErrorRow = {"0": [56], "10L": [55], "20L": [54], "30L": [53], "45L": [52], "60L": [51], "10R": [57], "20R": [58],"30R": [59], "45R": [60], "60R": [61]}
    def reader(self):
        for i in range(len(XLXSExport.availableDistances)):
            sensorDistance = XLXSExport.availableDistances[i]
            sensor = XLXSExport.availableCategories[0]
            sensorName = XLXSExport.avalailableSensors[0]
            angle = XLXSExport.availableAngles[0]
            rowrange = XLXSExport.rowrange[angle]
            colrange = XLXSExport.colrange[angle]
            end = True
            wb = Workbook()
            wb.create_sheet(sensorName)
            ws = wb[sensorName]
            avgDistances = []
            avgDeviations = []
            while end:
                path = 'Analyzed data2/CSVFiles/' + sensor + '/' + sensorName + '/' + sensorDistance + 'm/test' + sensorName + ' - ' + angle + ' angle - ' + sensorDistance + 'm_DistanceDeviationAvgCSV.csv'
                # print(path)
                try:
                    distance,deviation = XLXSExport.readCSV(path)
                    if len(distance) != 40 and len(deviation) != 40:
                        print("Not enough values:")
                        raise Exception("Not enough values")
                except:
                    print("File not found: " + path)
                    angle = XLXSExport.nextAngle(angle)
                    rowrange = XLXSExport.nextRowRange(angle)
                    colrange = XLXSExport.nextColRange(angle)
                    if angle == XLXSExport.availableAngles[0]:
                        wb.save("Analyzed data2/Automated/Automated "+str(sensorDistance)+"m.xlsx")
                        sensorName = XLXSExport.nextSensorName(sensorName)
                        wb.create_sheet(sensorName)
                        ws = wb[sensorName]
                        if sensorName == "b2" or sensorName == "S1" or sensorName == "P1":
                            sensor = XLXSExport.nextSensor(sensor)
                        if sensorName == "G1" and sensor == "Ping":
                            end = False
                    XLXSExport.generateDevOfDevChart(ws, sensorDistance)
                    XLXSExport.generateAbsErrorChart(ws, sensorDistance)
                    XLXSExport.generateRelevantErrorChart(ws,sensorDistance)
                    XLXSExport.generateAvgDevChart(ws, sensorDistance)
                    XLXSExport.generateAvgDistChart(ws, sensorDistance)
                    wb.save("Analyzed data2/Automated/Automated " + str(sensorDistance) + "m.xlsx")
                    continue
                ws[get_column_letter(colrange[0]) + str(rowrange[0] - 1)] = sensorName + '-' + angle + ' angle - ' + sensorDistance + 'm - 15ms wait - 15μs trigger- 100 reps'
                ws.merge_cells(get_column_letter(colrange[0]) + str(rowrange[0] - 1)+":"+get_column_letter(colrange[1]) + str(rowrange[0] - 1))
                pos = 0
                for col in range(colrange[0], colrange[1], 2):
                    currentDistance = 0.0
                    currectDeviation = 0.0
                    for row in range(rowrange[0], rowrange[1]):
                        char = get_column_letter(col)
                        nextChar = get_column_letter(col + 1)
                        ws[char + str(row)] = distance[pos]
                        ws[nextChar + str(row)] = deviation[pos]
                        currentDistance += float(distance[pos])
                        currectDeviation += float(deviation[pos])
                        pos += 1
                        if pos % 10 == 0:
                            avgDistances.append(round(currentDistance/10,3))
                            avgDeviations.append(round(currectDeviation/10,3))
                XLXSExport.exportAVg(ws,angle,XLXSExport.colrange[XLXSExport.availableAngles[6]],avgDistances,avgDeviations,sensorDistance)
                XLXSExport.exportDevOfDev(ws, angle, XLXSExport.colrange[XLXSExport.availableAngles[6]], sensorDistance)
                XLXSExport.exportAbsError(ws, angle, XLXSExport.colrange[XLXSExport.availableAngles[6]], sensorDistance)
                XLXSExport.exportRelevantError(ws, angle, XLXSExport.colrange[XLXSExport.availableAngles[6]], sensorDistance)
                avgDistances =[]
                avgDeviations =[]
                angle = XLXSExport.nextAngle(angle)
                rowrange = XLXSExport.nextRowRange(angle)
                colrange = XLXSExport.nextColRange(angle)
                path = 'Analyzed data2/CSVFiles/' + sensor + '/' + sensorName + '/' + sensorDistance + 'm/test'+sensorName+' - ' + angle + ' angle - ' + sensorDistance + 'm_DistanceDeviationAvgCSV.csv'
                if angle == XLXSExport.availableAngles[0]:
                    XLXSExport.generateDevOfDevChart(ws,sensorDistance)
                    XLXSExport.generateAbsErrorChart(ws,sensorDistance)
                    XLXSExport.generateRelevantErrorChart(ws,sensorDistance)
                    XLXSExport.generateAvgDevChart(ws,sensorDistance)
                    XLXSExport.generateAvgDistChart(ws,sensorDistance)
                    wb.save("Analyzed data2/Automated/Automated "+str(sensorDistance)+"m.xlsx")
                    sensorName = XLXSExport.nextSensorName(sensorName)
                    wb.create_sheet(sensorName)
                    ws = wb[sensorName]
                    if sensorName == "b2" or sensorName == "S1" or sensorName == "P1":
                        sensor = XLXSExport.nextSensor(sensor)
                    if sensorName == "G1" and sensor=="Ping":
                        end = False
            XLXSExport.GenerateGroupChart(wb,sensorDistance)
            wb.save("Analyzed data2/Automated/Automated "+str(sensorDistance)+"m.xlsx")




    def nextAngle(self):
        for i in range(len(XLXSExport.availableAngles)-1):
            if XLXSExport.availableAngles[i] == self and self != "60R":
                return XLXSExport.availableAngles[i+1]
        return XLXSExport.availableAngles[0]

    def nextRowRange(self):
       return XLXSExport.rowrange[self]

    def nextColRange(self):
        return XLXSExport.colrange[self]

    def nextSensorName(self):
        for i in range(len(XLXSExport.avalailableSensors) - 1):
            if XLXSExport.avalailableSensors[i] == self and self != "60R":
                return XLXSExport.avalailableSensors[i + 1]
        return XLXSExport.avalailableSensors[0]

    def nextSensor(self):
        for i in range(len(XLXSExport.availableCategories) - 1):
            if XLXSExport.availableCategories[i] == self and self != "60R":
                return XLXSExport.availableCategories[i + 1]
        return XLXSExport.availableCategories[0]

    def readCSV(self):
        with open(self) as csv_file:
            csv_reader = csv.reader(csv_file)
            lineCount = 1
            distance = []
            deviation = []
            for row in csv_reader:
                if len(row) > 1:
                    lineCount += 1
                    if lineCount % 11 != 1:
                        distance.append(row[0])
                        deviation.append(row[1])
        return distance,deviation

    def exportAVg(ws,angle,colrange,avgDistance,avgDeviation,sensorDistance):
        pos=0
        row = XLXSExport.avgRow[angle]
        ws[get_column_letter(colrange[0]+9) + str(row[0]+2)] = angle
        ws[get_column_letter(colrange[0]+10) + str(1)] = "Μέση Τυπική Απόκλιση/Απόσταση ανά γωνία για "+sensorDistance+"m"
        ws.merge_cells(get_column_letter(colrange[0]+10) + str(1)+":"+get_column_letter(colrange[1]+10) + str(1))
        for col in range(colrange[0]+10, colrange[1]+10, 2):
            ws[get_column_letter(col) + str(3)] = "test"+str(pos+1)
            ws[get_column_letter(col+1) + str(3)] = "test" + str(pos + 1)
            # ws.merge_cells(get_column_letter(col) + str(2)+":"+get_column_letter(col+1) + str(2))
            ws[get_column_letter(col) + str(2)] = "Μέση Απόσταση"
            ws[get_column_letter(col+1) + str(2)] = "Μέση Τυπική Απόκλιση"
            char = get_column_letter(col)
            nextChar = get_column_letter(col + 1)
            ws[char + str(row[0]+2)] = avgDistance[pos]
            ws[nextChar + str(row[0]+2)] = avgDeviation[pos]
            pos += 1

    def exportDevOfDev(ws,angle,colrange,sensorDistance):
        pos=0
        row = XLXSExport.avgDevRow[angle]
        ws[get_column_letter(colrange[0]+9) + str(row[0]+2)] = angle
        ws[get_column_letter(colrange[0]+10) + str(17)] = "Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης "+sensorDistance+"m"
        ws.merge_cells(get_column_letter(colrange[0]+10) + str(17)+":"+get_column_letter(colrange[1]+6) + str(17))
        for col in range(colrange[0]+10, colrange[1]+7):
            ws[get_column_letter(col) + str(19)] = "test"+str(pos+1)
            ws[get_column_letter(col) + str(18)] = "Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης"
            char = get_column_letter(col)
            ws[char + str(row[0]+2)] = XLXSExport.getSTDev(ws,angle,pos)
            pos += 1

    def getSTDev(ws,angle,pos):
            posMatch= {0: [XLXSExport.colrange[angle][0]+1], 1: [XLXSExport.colrange[angle][0]+3], 2: [XLXSExport.colrange[angle][0]+5], 3: [XLXSExport.colrange[angle][0]+7]}
            deviation = []
            for row in range(XLXSExport.rowrange[angle][0]-1,XLXSExport.rowrange[angle][1]-1):
                deviation.append(Decimal(ws[get_column_letter(posMatch[pos][0])][row].value))
            stdDev = statistics.stdev(deviation)
            return round(stdDev,3)


    def exportAbsError(ws,angle,colrange,sensorDistance):
        pos=0
        row = XLXSExport.absErrorRow[angle]
        ws[get_column_letter(colrange[0]+9) + str(row[0]+2)] = angle
        ws[get_column_letter(colrange[0]+10) + str(33)] = "Απόλυτο σφάλμα "+sensorDistance+"m"
        ws.merge_cells(get_column_letter(colrange[0]+10) + str(33)+":"+get_column_letter(colrange[1]+6) + str(33))
        for col in range(colrange[0]+10, colrange[1]+7):
            ws[get_column_letter(col) + str(35)] = "test"+str(pos+1)
            ws[get_column_letter(col) + str(34)] = "Απόλυτο σφάλμα"
            char = get_column_letter(col)
            ws[char + str(row[0]+2)] = XLXSExport.getAbsError(ws,angle,pos,row[0]+2,sensorDistance)
            pos += 1

    def getAbsError(ws,angle,pos,currentRow,sensorDistance):
        posMatch = {0: [21], 1: [23],2: [25], 3: [27]}
        distanceMatch = {"0.5":[50],"1":[100],"2":[200],"3":[300],"4":[400]}
        return round(abs(Decimal(ws[get_column_letter(posMatch[pos][0])][currentRow-33].value) - distanceMatch[sensorDistance][0]),3)

    def exportRelevantError(ws,angle,colrange,sensorDistance):
        pos=0
        row = XLXSExport.relErrorRow[angle]
        ws[get_column_letter(colrange[0]+9) + str(row[0]+2)] = angle
        ws[get_column_letter(colrange[0]+10) + str(50)] = "Σχετικό Σφάλμα (%) "+sensorDistance+"m"
        ws.merge_cells(get_column_letter(colrange[0]+10) + str(50)+":"+get_column_letter(colrange[1]+6) + str(50))
        for col in range(colrange[0]+10, colrange[1]+7):
            ws[get_column_letter(col) + str(52)] = "test"+str(pos+1)
            ws[get_column_letter(col) + str(51)] = "Σχετικό Σφάλμα (%)"
            char = get_column_letter(col)
            ws[char + str(row[0]+2)] = XLXSExport.getRelevantError(ws,angle,pos,row[0]+2,sensorDistance)
            pos += 1

    def getRelevantError(ws,angle,pos,currentRow,sensorDistance):
        posMatch = {0: [21], 1: [23],2: [25], 3: [27]}
        distanceMatch = {"0.5":[50],"1":[100],"2":[200],"3":[300],"4":[400]}
        # value = ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value
        distance = distanceMatch[sensorDistance][0]
        test0 =  ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value
        test1 = ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value - distanceMatch[sensorDistance][0]
        ast = (ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value - distanceMatch[sensorDistance][0])/distanceMatch[sensorDistance][0]
        test = round((Decimal(abs(ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value - distanceMatch[sensorDistance][0]))/distanceMatch[sensorDistance][0])*100,3)
        return round((Decimal(abs(ws[get_column_letter(posMatch[pos][0])][currentRow - 50].value - distanceMatch[sensorDistance][0]))/distanceMatch[sensorDistance][0])*100,3)

    def generateDevOfDevChart(ws,distance):
        chart = LineChart()
        chart.title="Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης (cm) / Γωνία (deg) για "+distance+"m"
        chart.y_axis.title ="Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης (cm)"
        chart.x_axis.title ="Γωνία (deg)"
        chart.y_axis.scaling.max = 0.2
        chart.y_axis.scaling.min = 0
        data=Reference(ws,min_col=21,min_row=19,max_col=24,max_row=30)
        chart.add_data(data,titles_from_data=True)
        xaxis= Reference(ws, min_col=20,max_col=20,min_row=20, max_row=30)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0,4):
            yk=chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chart,"AB17")


    def generateAbsErrorChart(ws,distance):
        chartAbs = LineChart()
        chartAbs.title="Απόλυτο σφάλμα (cm) / Γωνία (deg) για "+distance+"m"
        chartAbs.y_axis.title ="Απόλυτο σφάλμα (cm)"
        chartAbs.x_axis.title ="Γωνία (deg)"
        chartAbs.y_axis.scaling.max = float(distance)*10
        chartAbs.y_axis.scaling.min = 0
        dataAbs=Reference(ws,min_col=21,min_row=35,max_col=24,max_row=46)
        chartAbs.add_data(dataAbs,titles_from_data=True)
        xaxis= Reference(ws, min_col=20,max_col=20,min_row=36, max_row=46)
        chartAbs.set_categories(xaxis)
        chartAbs.display_blank = 'gap'
        for k in range(0,4):
            yk=chartAbs.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chartAbs,"AB33")

    def generateRelevantErrorChart(ws,distance):
        chartAbs = LineChart()
        chartAbs.title="Σχετικό Σφάλμα (%) / Γωνία (deg) για "+distance+"m"
        chartAbs.y_axis.title ="Σχετικό Σφάλμα (%)"
        chartAbs.x_axis.title ="Γωνία (deg)"
        chartAbs.y_axis.scaling.max = float(distance)*10
        chartAbs.y_axis.scaling.min = 0
        dataAbs=Reference(ws,min_col=21,min_row=52,max_col=24,max_row=63)
        chartAbs.add_data(dataAbs,titles_from_data=True)
        xaxis= Reference(ws, min_col=20,max_col=20,min_row=53, max_row=63)
        chartAbs.set_categories(xaxis)
        chartAbs.display_blank = 'gap'
        for k in range(0,4):
            yk=chartAbs.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chartAbs,"AB50")

    def generateAvgDevChart(ws, distance):
        chart = LineChart()
        chart.title = "Μέση Τυπική Απόκλιση (cm) / Γωνία (deg) για " + distance + "m"
        chart.y_axis.title = "Μέση Τυπική Απόκλιση (cm)"
        chart.x_axis.title = "Γωνία (deg)"
        chart.y_axis.scaling.max = float(distance) * 1.5
        chart.y_axis.scaling.min = 0
        xaxis = Reference(ws, min_col=20, max_col=20, min_row=4, max_row=14)
        data = Reference(ws, min_col=22, min_row=3, max_col=22, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=24, min_row=3, max_col=24, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=26, min_row=3, max_col=26, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=28, min_row=3, max_col=28, max_row=14)
        chart.add_data(data, titles_from_data=True)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0,4):
            yk=chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth = True
        ws.add_chart(chart, "AE1")

    def generateAvgDistChart(ws, distance):
        chart = LineChart()
        chart.title = "Μέση Απόσταση (cm) / Γωνία (deg) for " + distance + "m"
        chart.y_axis.title = "Μέση Απόσταση (cm)"
        chart.x_axis.title = "Γωνία (deg)"
        chart.y_axis.scaling.max = float(distance) * 200
        chart.y_axis.scaling.min = 0
        xaxis = Reference(ws, min_col=20, max_col=20, min_row=4, max_row=14)
        data = Reference(ws, min_col=21, min_row=3, max_col=21, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=23, min_row=3, max_col=23, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=23, min_row=3, max_col=23, max_row=14)
        chart.add_data(data, titles_from_data=True)
        data = Reference(ws, min_col=23, min_row=3, max_col=23, max_row=14)
        chart.add_data(data, titles_from_data=True)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0, 4):
            yk = chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth = True
        ws.add_chart(chart, "AO1")


    def GenerateGroupChart(self,sensorDistance):
        print('Generating sumurized files....')
        titles = ["Απόλυτο σφάλμα (cm) / Γωνία (deg) για "+sensorDistance+"m",
                  "Σχετικό Σφάλμα (%) / Γωνία (deg) για "+sensorDistance+"m",
                  "Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης (cm) / Γωνία (deg) για " + sensorDistance + "m",
                  "Μέση Τυπική Απόκλιση (cm) / Γωνία (deg) για " + sensorDistance + "m",
                  "Μέση Απόσταση (cm) / Γωνία (deg) for " + sensorDistance + "m"]
        title = 4
        self.remove_sheet(self.get_sheet_by_name('Sheet'))
        self.remove_sheet(self.get_sheet_by_name('G11'))
        for x in XLXSExport.availableCategories:
            ws = self.create_sheet(x)
            for x in range (1,66,13):
                ws.cell(row=x, column=1, value=titles[title])
                ws.merge_cells(get_column_letter(1) + str(x)+":"+get_column_letter(6) + str(x))
                title-=1
                for y in range (0,5):
                    ws[get_column_letter(2+y) + str(x+1)] = "test" + str(y+1)
                for y in range (0,11):
                    if y == 0:
                        ws[get_column_letter(1) + str(x+7)] = XLXSExport.availableAngles[y]
                    elif y > 0 and y <6:
                        ws[get_column_letter(1)+ str(x+7-y)] = XLXSExport.availableAngles[y]
                    else:
                        ws[get_column_letter(1) + str(x+2+y)] = XLXSExport.availableAngles[y]
                self.save("Analyzed data2/Automated/Automated " + str(sensorDistance) + "m.xlsx")
            XLXSExport.generateAvgDistChartSUM(ws, sensorDistance)
            XLXSExport.generateAvgDevChartSUM(ws, sensorDistance)
            XLXSExport.generateDevOfDevChartSUM(ws, sensorDistance)
            XLXSExport.generateRelevantErrorChartSUM(ws, sensorDistance)
            XLXSExport.generateAbsErrorChartSUM(ws, sensorDistance)
            title = 4
        self.save("Analyzed data2/Automated/Automated " + str(sensorDistance) + "m.xlsx")
        print(".....Completed")

    def generateAvgDistChartSUM(ws, distance):
        chart = LineChart()
        chart.title = "Μέση Απόσταση (cm) / Γωνία (deg) for " + distance + "m"
        chart.y_axis.title = "Μέση Απόσταση (cm)"
        chart.x_axis.title = "Γωνία (deg)"
        chart.y_axis.scaling.max = float(distance) * 200
        chart.y_axis.scaling.min = 0
        xaxis = Reference(ws, min_col=1, max_col=1, min_row=3, max_row=13)
        data = Reference(ws, min_col=2, min_row=2, max_col=6, max_row=13)
        chart.add_data(data, titles_from_data=True)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0, 5):
            yk = chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth = True
        ws.add_chart(chart, "I1")

    def generateAvgDevChartSUM(ws, distance):
        chart = LineChart()
        chart.title = "Μέση Τυπική Απόκλιση (cm) / Γωνία (deg) για " + distance + "m"
        chart.y_axis.title = "Μέση Τυπική Απόκλιση (cm)"
        chart.x_axis.title = "Γωνία (deg)"
        chart.y_axis.scaling.max = float(distance) * 1.5
        chart.y_axis.scaling.min = 0
        xaxis = Reference(ws, min_col=1, max_col=1, min_row=16, max_row=26)
        data = Reference(ws, min_col=2, min_row=15, max_col=6, max_row=26)
        chart.add_data(data, titles_from_data=True)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0, 5):
            yk = chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth = True
        ws.add_chart(chart, "I15")

    def generateDevOfDevChartSUM(ws,distance):
        chart = LineChart()
        chart.title="Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης (cm) / Γωνία (deg) για "+distance+"m"
        chart.y_axis.title ="Μέση Τυπική Απόκλιση της Μέσης Τυπικής Απόκλισης (cm)"
        chart.x_axis.title ="Γωνία (deg)"
        chart.y_axis.scaling.max = 0.2
        chart.y_axis.scaling.min = 0
        data=Reference(ws, min_col=2, min_row=28, max_col=6, max_row=39)
        chart.add_data(data,titles_from_data=True)
        xaxis= Reference(ws, min_col=1, max_col=1, min_row=29, max_row=39)
        chart.set_categories(xaxis)
        chart.display_blank = 'gap'
        for k in range(0,5):
            yk=chart.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chart,"I28")

    def generateRelevantErrorChartSUM(ws,distance):
        chartAbs = LineChart()
        chartAbs.title="Σχετικό Σφάλμα (%) / Γωνία (deg) για "+distance+"m"
        chartAbs.y_axis.title ="Σχετικό Σφάλμα (%)"
        chartAbs.x_axis.title ="Γωνία (deg)"
        chartAbs.y_axis.scaling.max = float(distance)*10
        chartAbs.y_axis.scaling.min = 0
        dataAbs=Reference(ws, min_col=2, min_row=41, max_col=6, max_row=52)
        chartAbs.add_data(dataAbs,titles_from_data=True)
        xaxis= Reference(ws, min_col=1, max_col=1, min_row=42, max_row=52)
        chartAbs.set_categories(xaxis)
        chartAbs.display_blank = 'gap'
        for k in range(0,5):
            yk=chartAbs.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chartAbs,"I41")

    def generateAbsErrorChartSUM(ws,distance):
        chartAbs = LineChart()
        chartAbs.title="Απόλυτο σφάλμα (cm) / Γωνία (deg) για "+distance+"m"
        chartAbs.y_axis.title ="Απόλυτο σφάλμα (cm)"
        chartAbs.x_axis.title ="Γωνία (deg)"
        chartAbs.y_axis.scaling.max = float(distance)*10
        chartAbs.y_axis.scaling.min = 0
        dataAbs=Reference(ws, min_col=2, min_row=54, max_col=6, max_row=65)
        chartAbs.add_data(dataAbs,titles_from_data=True)
        xaxis= Reference(ws, min_col=1, max_col=1, min_row=55, max_row=65)
        chartAbs.set_categories(xaxis)
        chartAbs.display_blank = 'gap'
        for k in range(0,5):
            yk=chartAbs.series[k]
            yk.marker.symbol = "square"
            yk.smooth=True
        ws.add_chart(chartAbs,"I54")