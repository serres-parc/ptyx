# import RPi.GPIO as GPIO
import time


class Ping:

    def distance(self, PORT, TICKS, TEMPERATURE, triggerTime):
        GPIO.setmode(GPIO.BOARD)
        measurements = []
        speedOfSound = (331.3 + 0.606 * TEMPERATURE) * 100
        for i in range(TICKS):
            GPIO.setup(PORT, GPIO.OUT)
            GPIO.output(PORT, 1)
            time.sleep(triggerTime)
            GPIO.output(PORT, 0)
            GPIO.setup(PORT, GPIO.IN)
            startTime = time.time()
            stopTime = time.time()
            while GPIO.input(PORT) == 0:
                startTime = time.time()
            while GPIO.input(PORT) == 1:
                stopTime = time.time()
            timeEl = stopTime - startTime
            distance = (timeEl * speedOfSound) / 2
            measurements.append(distance)
        averageDist = sum(measurements) / len(measurements)
        GPIO.cleanup()
        return averageDist