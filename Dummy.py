class Dummy:
    data = 0.0

    def __init__(self):
        self.data = [100.6356, 100.4219, 99.9982, 50.5419, 101.009]

    def getDummyData(self):
        return self.data

    def getDataSum(self):
        return sum(self.data)

    def getDataSumCustom(self):
        sum = 0.0
        for i in self.data:
            sum += i
        return sum

    def pop(self):
        print(self.data)
        self.data.pop()
        print(self.data)
