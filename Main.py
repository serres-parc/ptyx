import time 
# import DistanceSensor
import Calc 
import Display 
import IO 
# import Ping
import ArduinoConnect
import XLSXExport

#test

TRIGGER = 12
ECHO = 18
REPS = 100
waitTime = 0.015  # 0.005
triggerTime = 0.000015  # 0.000010
name = "P2 - 0 angle - 4m"
testNumber = 0
file = "test" + str(testNumber) + str(name) + "_" + str(waitTime * 1000) + "ms-trig" + str(
    triggerTime * 1000000) + "micros_" + str(
    REPS) + "reps.txt"
rawFile = "test" + str(testNumber) + str(name) + "Raw_" + str(waitTime * 1000) + "ms-trig" + str(
    triggerTime * 1000000) + "micros_" + str(
    REPS) + "reps.txt"
fileCSV = "test" + str(name) + "_""DistanceDeviationAvgCSV.csv"
temperature = 22

# distMes = DistanceSensor.DistanceSensor()
calculation = Calc.Calc()
display = Display.UI()
io = IO.IO(file)
ioRaw = IO.IO(rawFile)
ioCSV = IO.IO(fileCSV)
# ping = Ping.Ping()
xlsx = XLSXExport.XLXSExport()

measurements = []
deviations = []
avgDist = []
average = 0.0
counter = 0
errorCounter = 0
passedCounter = 0
distance = 0.0

if __name__ == '__main__':
    try:
        usercase ='2' #'test'
        while usercase != "1" and usercase!="2" and usercase!="0":
            usercase =input("press 1 to get new measurements\npress 2 to analyze existing measurements\npress 0 to exit")
        # test = True
        if usercase=="1":
            nextReps = 0
            # while testNumber < 4:  # while test:
            #     if nextReps != testNumber:  # REPS:
            #         ioCSV.writeToFile(fileCSV, "wait time: " + str(waitTime * 1000) + " ms trigger time: " + str(
            #             triggerTime * 1000000) + " micros reps: " + str(REPS) + "\n")
            #         ioCSV.writeToFile(fileCSV, "Distance, Deviation\n")
            #         nextReps = testNumber
            #     # arduino = ArduinoConnect.Arduino()
            #     errorCounter = 0
            #     passedCounter = 0
            #     del measurements[:]
            #     ioRaw.writeToFile(rawFile,
            #                       str('\nBlock: ' + str(counter + 1) + '\n======================================\n'))
            #     io.writeToFile(file, str('\nBlock: ' + str(counter + 1) + '\n======================================\n'))
            #     # distances = arduino.getArduinoDistance(REPS)
            #     for x in range(REPS):
            #         distance = x
            #         distance = ping.distance(TRIGGER, 1, temperature, triggerTime)  # Ping Sensor callout
            #         # distance = distMes.distance(TRIGGER, ECHO, 1, temperature, triggerTime)
            #         ioRaw.writeToFile(rawFile, str(distance) + '\n')
            #         measurements.append(distance)
            #         passedCounter += 1
            #         io.writeToFile(file, str(distance) + '\n')
            #         time.sleep(waitTime)
            #     average = 0.0
            #     deviation = 0.0
            #     average = calculation.findAverage(measurements)
            #     deviation = calculation.deviation(measurements)
            #     display.showCustomMessage(
            #         str('\nBlock: ' + str(counter + 1) + '\n======================================\n'))
            #     display.showAverage(average)
            #     display.showDeviation(deviation)
            #     deviations.append(deviation)
            #     avgDist.append(average)
            #     print("Measurements taken: ", len(measurements))
            #     io.writeToFile(file, str('\nAverage distance is : %.3f cm' % average))
            #     io.writeToFile(file, str('\nAverage deviation is : %.3f cm' % deviation))
            #     ioCSV.writeToFile(fileCSV, "%.3f," % average)
            #     ioCSV.writeToFile(fileCSV, "%.3f\n" % deviation)
            #     counter += 1
            #     if counter == 10:
            #         counter = 0
            #         # REPS = io.repeater(REPS)
            #         # counter = 0
            #         # if REPS == 5:
            #         #     waitTime = io.changeWaitTime(waitTime)
            #         #     if waitTime == 0.005:
            #         #         triggerTime = io.changeTriggerTime(triggerTime)
            #         #         if triggerTime == 0.000010:
            #         #             display.showFinished()
            #         #             time.sleep(1800)
            #         testNumber += 1
            #         file = "test" + str(testNumber) + str(name) + "_" + str(waitTime * 1000) + "ms-trig" + str(
            #             triggerTime * 1000000) + "micros_" + str(REPS) + "reps.txt"
            #         rawFile = "test" + str(testNumber) + str(name) + "Raw_" + str(waitTime * 1000) + "ms-trig" + str(
            #             triggerTime * 1000000) + "micros_" + str(REPS) + "reps.txt"
            #     time.sleep(0.5)
            # display.showFinished()
        elif usercase == "2":
            display.showCustomMessage("Analysing...")
            xlsx.reader()
        else:
            quit()
    except KeyboardInterrupt:
        print('Stopped measurement')
