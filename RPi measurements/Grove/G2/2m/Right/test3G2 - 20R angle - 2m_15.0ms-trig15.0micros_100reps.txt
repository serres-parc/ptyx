
Block: 1
======================================
519.68552072
519.631642818
202.303233218
201.79760983
202.2327775
201.8183321
202.236921954
202.2327775
519.789132071
519.805709887
519.789132071
519.805709887
202.651367354
202.2327775
202.66794517
202.672089624
202.236921954
202.249355316
202.2327775
202.25349977
202.25349977
202.236921954
519.843009973
519.768409801
519.755976439
519.772554255
519.755976439
519.789132071
202.651367354
519.772554255
202.216199684
202.655511808
519.789132071
519.772554255
519.772554255
519.772554255
519.772554255
519.809854341
201.8183321
519.793276525
202.236921954
519.789132071
519.772554255
519.789132071
519.789132071
519.755976439
519.805709887
519.772554255
519.68552072
519.789132071
519.805709887
519.789132071
519.789132071
519.789132071
519.805709887
202.236921954
202.216199684
202.216199684
201.785176468
201.781032014
202.216199684
201.781032014
202.199621868
202.203766322
202.199621868
202.655511808
202.216199684
202.651367354
202.597489452
202.634789538
519.772554255
519.805709887
519.772554255
519.772554255
519.772554255
202.199621868
202.634789538
202.199621868
519.789132071
519.768409801
202.183044052
202.634789538
202.618211722
202.183044052
519.772554255
519.789132071
519.789132071
519.772554255
519.772554255
519.772554255
519.772554255
519.793276525
519.805709887
519.859587789
519.805709887
202.634789538
519.789132071
519.789132071
519.789132071
519.805709887

Average distance is : 380.085 cm
Average deviation is : 158.392 cm
Block: 2
======================================
519.805709887
519.805709887
519.735254169
519.668942904
519.668942904
519.789132071
519.809854341
519.789132071
519.789132071
519.789132071
519.809854341
519.809854341
519.963199139
519.805709887
519.789132071
519.822287703
519.772554255
519.805709887
519.789132071
519.805709887
519.805709887
519.805709887
519.789132071
519.805709887
519.789132071
519.805709887
519.789132071
202.684522986
201.8183321
201.834909916
201.801754284
201.834909916
201.814187646
201.834909916
201.8183321
201.8183321
201.8183321
201.8183321
201.79760983
201.79760983
201.79760983
201.8183321
201.801754284
201.4038867
201.8183321
201.79760983
201.834909916
202.25349977
201.814187646
202.249355316
202.25349977
202.270077586
201.834909916
201.8183321
202.236921954
201.801754284
201.8183321
201.79760983
201.814187646
201.8183321
202.216199684
202.216199684
201.814187646
201.801754284
202.236921954
201.79760983
202.2327775
202.216199684
202.2327775
519.755976439
519.789132071
519.843009973
519.826432157
519.789132071
519.826432157
519.826432157
519.843009973
519.826432157
519.859587789
202.199621868
202.66794517
202.66794517
202.220344138
202.236921954
202.216199684
202.216199684
202.236921954
202.66794517
202.236921954
202.220344138
202.651367354
519.838865519
202.220344138
202.634789538
202.220344138
202.216199684
202.220344138
201.8183321
201.781032014
201.781032014

Average distance is : 322.791 cm
Average deviation is : 155.013 cm
Block: 3
======================================
519.735254169
519.718676353
519.702098536
519.789132071
519.843009973
519.822287703
519.822287703
519.876165605
519.838865519
519.838865519
519.805709887
519.809854341
202.68866744
202.286655402
201.8183321
202.25349977
201.855632186
202.270077586
202.236921954
202.236921954
202.270077586
202.236921954
201.8183321
201.8183321
202.25349977
201.8183321
201.834909916
201.8183321
201.834909916
201.834909916
201.834909916
201.8183321
201.834909916
201.834909916
201.851487732
201.814187646
201.851487732
202.25349977
201.834909916
519.809854341
519.805709887
519.809854341
519.789132071
519.809854341
519.789132071
519.826432157
519.822287703
519.843009973
519.843009973
519.859587789
519.843009973
519.843009973
519.822287703
519.822287703
519.805709887
519.826432157
519.805709887
519.805709887
519.826432157
519.822287703
519.822287703
519.843009973
519.822287703
519.822287703
519.668942904
519.826432157
519.843009973
519.843009973
202.25349977
202.25349977
201.834909916
201.8183321
201.8183321
201.814187646
201.79760983
201.8183321
201.8183321
201.801754284
201.38316443
201.8183321
201.8183321
201.8183321
201.801754284
201.8183321
201.8183321
202.2327775
202.25349977
202.684522986
202.2327775
519.838865519
519.843009973
519.822287703
519.843009973
519.843009973
519.843009973
519.68552072
519.843009973
519.892743421
519.913465691
202.68866744

Average distance is : 364.082 cm
Average deviation is : 159.682 cm
Block: 4
======================================
519.855443335
519.822287703
519.805709887
519.843009973
519.822287703
519.739398623
519.718676353
519.772554255
519.859587789
519.843009973
519.822287703
519.859587789
519.843009973
519.855443335
519.843009973
519.838865519
519.826432157
519.855443335
519.859587789
519.859587789
519.838865519
519.843009973
519.826432157
519.822287703
519.826432157
519.805709887
519.789132071
519.826432157
519.843009973
519.822287703
519.826432157
519.805709887
519.809854341
202.68866744
519.789132071
202.68866744
202.2327775
201.834909916
519.805709887
519.805709887
519.789132071
519.826432157
519.793276525
519.826432157
519.772554255
519.789132071
519.789132071
519.809854341
519.805709887
519.843009973
519.843009973
519.789132071
519.789132071
519.768409801
519.772554255
519.805709887
519.826432157
519.826432157
519.809854341
519.826432157
519.805709887
519.822287703
519.843009973
519.826432157
519.822287703
519.805709887
519.822287703
519.805709887
519.805709887
519.805709887
519.859587789
519.826432157
519.838865519
519.843009973
519.822287703
519.805709887
519.822287703
519.843009973
519.826432157
519.805709887
519.826432157
519.843009973
519.826432157
519.859587789
519.822287703
519.822287703
519.838865519
519.822287703
519.826432157
519.826432157
519.822287703
519.822287703
519.805709887
519.838865519
519.838865519
519.822287703
519.822287703
519.822287703
519.822287703
519.843009973

Average distance is : 507.122 cm
Average deviation is : 62.522 cm
Block: 5
======================================
519.772554255
519.735254169
519.739398623
519.751831985
519.843009973
519.855443335
519.805709887
519.838865519
519.805709887
519.805709887
519.805709887
519.789132071
202.270077586
202.705245256
202.705245256
202.286655402
202.286655402
202.303233218
519.793276525
202.68866744
519.826432157
519.822287703
202.290799856
202.270077586
201.868065548
201.855632186
201.834909916
201.855632186
201.851487732
201.834909916
201.855632186
201.851487732
201.851487732
201.851487732
201.851487732
201.851487732
201.868065548
201.851487732
201.834909916
201.872210002
201.855632186
201.855632186
201.851487732
202.286655402
201.851487732
202.286655402
202.270077586
202.270077586
202.25349977
202.286655402
202.286655402
202.286655402
202.286655402
202.270077586
202.307377672
202.286655402
202.307377672
202.303233218
202.721823072
202.721823072
202.742545342
519.772554255
202.307377672
519.843009973
519.843009973
519.805709887
519.843009973
519.838865519
519.838865519
519.822287703
519.838865519
519.822287703
519.822287703
519.826432157
519.826432157
519.822287703
519.843009973
519.843009973
519.896887875
519.880310059
519.843009973
519.809854341
519.809854341
519.809854341
519.805709887
519.789132071
519.768409801
519.805709887
202.323955488
519.805709887
202.754978704
519.789132071
519.789132071
519.805709887
519.826432157
519.789132071
519.805709887
519.789132071
519.805709887
519.789132071

Average distance is : 364.169 cm
Average deviation is : 159.589 cm
Block: 6
======================================
519.722820807
519.735254169
519.735254169
519.755976439
519.826432157
519.838865519
519.843009973
519.838865519
519.843009973
519.859587789
519.859587789
519.822287703
519.838865519
519.822287703
519.805709887
519.822287703
519.822287703
519.838865519
519.789132071
519.826432157
519.822287703
519.822287703
519.789132071
519.822287703
519.822287703
519.876165605
519.892743421
519.859587789
519.909321237
519.930043507
519.876165605
519.892743421
519.876165605
519.876165605
519.876165605
519.876165605
519.876165605
519.876165605
519.876165605
519.892743421
519.859587789
202.323955488
202.754978704
202.319811034
519.876165605
519.876165605
519.876165605
519.876165605
519.876165605
519.876165605
519.859587789
519.876165605
519.896887875
519.913465691
519.876165605
519.892743421
519.896887875
519.896887875
519.876165605
519.859587789
519.843009973
519.859587789
519.843009973
519.880310059
519.876165605
519.826432157
519.838865519
519.843009973
519.859587789
519.859587789
519.859587789
519.876165605
519.896887875
519.843009973
519.843009973
519.876165605
519.838865519
519.838865519
519.843009973
519.859587789
519.843009973
519.859587789
519.855443335
519.876165605
519.859587789
519.876165605
519.876165605
519.859587789
519.876165605
519.855443335
519.925899053
519.925899053
519.843009973
519.876165605
519.838865519
519.859587789
519.859587789
519.855443335
519.822287703
519.843009973

Average distance is : 510.333 cm
Average deviation is : 54.415 cm
Block: 7
======================================
519.876165605
519.838865519
519.826432157
519.735254169
519.718676353
519.859587789
519.859587789
519.859587789
519.859587789
519.859587789
519.859587789
519.892743421
519.876165605
519.838865519
519.859587789
519.843009973
519.859587789
519.843009973
519.843009973
519.826432157
519.826432157
519.859587789
519.843009973
519.838865519
519.826432157
519.826432157
519.843009973
519.843009973
519.843009973
519.843009973
519.838865519
519.843009973
519.826432157
519.859587789
519.859587789
519.859587789
519.822287703
519.822287703
519.843009973
519.859587789
519.913465691
519.896887875
519.822287703
519.859587789
519.843009973
519.843009973
519.838865519
519.838865519
519.838865519
519.843009973
519.843009973
519.859587789
519.826432157
519.859587789
519.838865519
519.826432157
519.876165605
519.843009973
519.843009973
519.859587789
519.859587789
519.838865519
519.859587789
519.838865519
519.859587789
519.822287703
519.843009973
519.859587789
519.859587789
519.843009973
202.319811034
519.843009973
519.805709887
519.822287703
519.805709887
519.809854341
519.843009973
519.838865519
519.822287703
519.838865519
519.826432157
519.843009973
519.843009973
202.290799856
202.286655402
201.868065548
202.270077586
202.270077586
519.843009973
202.286655402
519.826432157
519.859587789
519.838865519
519.859587789
519.859587789
519.859587789
519.876165605
519.859587789
519.838865519
519.855443335

Average distance is : 497.611 cm
Average deviation is : 81.447 cm
Block: 8
======================================
519.822287703
519.822287703
519.843009973
519.859587789
519.822287703
519.789132071
519.718676353
519.718676353
519.822287703
519.805709887
519.822287703
519.843009973
519.822287703
519.838865519
519.843009973
519.826432157
519.843009973
519.838865519
519.805709887
519.843009973
519.826432157
519.826432157
519.822287703
519.838865519
519.822287703
519.822287703
519.822287703
519.822287703
519.859587789
202.25349977
519.805709887
519.789132071
519.822287703
519.826432157
519.809854341
519.805709887
519.843009973
519.822287703
519.826432157
519.805709887
519.838865519
519.843009973
519.809854341
519.822287703
519.826432157
519.805709887
519.843009973
519.826432157
202.701100802
202.270077586
201.834909916
202.25349977
201.834909916
202.270077586
201.834909916
202.286655402
202.303233218
202.286655402
202.25349977
202.270077586
202.68866744
202.249355316
519.789132071
519.805709887
519.789132071
519.789132071
202.705245256
202.286655402
202.286655402
202.270077586
202.705245256
519.809854341
202.721823072
519.789132071
519.789132071
202.286655402
202.270077586
202.705245256
519.789132071
202.290799856
519.822287703
202.270077586
202.270077586
202.270077586
202.270077586
201.868065548
202.290799856
201.868065548
202.286655402
202.286655402
202.303233218
202.286655402
519.805709887
519.838865519
519.805709887
519.826432157
519.805709887
519.805709887
519.789132071
519.822287703

Average distance is : 405.506 cm
Average deviation is : 153.181 cm
Block: 9
======================================
519.805709887
519.793276525
519.789132071
519.805709887
519.755976439
519.648220634
519.702098536
519.809854341
519.772554255
519.805709887
519.772554255
519.789132071
519.809854341
519.789132071
519.809854341
519.822287703
519.805709887
519.822287703
519.838865519
519.789132071
519.843009973
519.789132071
519.789132071
519.805709887
519.822287703
519.772554255
519.789132071
519.805709887
519.805709887
519.805709887
519.805709887
519.789132071
202.270077586
519.805709887
519.809854341
519.826432157
519.789132071
519.809854341
519.809854341
519.772554255
519.805709887
519.789132071
519.789132071
519.772554255
519.772554255
519.809854341
519.772554255
519.789132071
519.789132071
519.826432157
519.805709887
519.805709887
519.805709887
519.789132071
519.789132071
519.805709887
519.789132071
519.805709887
519.805709887
519.789132071
519.805709887
519.826432157
519.826432157
519.809854341
519.822287703
519.768409801
519.789132071
519.789132071
519.789132071
519.789132071
519.805709887
202.270077586
519.789132071
519.805709887
519.809854341
519.805709887
519.805709887
519.805709887
519.805709887
519.876165605
519.822287703
519.805709887
519.789132071
519.826432157
201.834909916
202.25349977
202.270077586
519.805709887
202.705245256
519.855443335
519.826432157
519.843009973
519.822287703
519.805709887
519.805709887
519.789132071
519.809854341
519.859587789
519.809854341
202.270077586

Average distance is : 497.574 cm
Average deviation is : 81.426 cm
Block: 10
======================================
519.739398623
519.68552072
519.668942904
202.216199684
202.25349977
202.2327775
202.216199684
201.801754284
201.8183321
201.801754284
201.8183321
201.8183321
201.8183321
201.79760983
201.8183321
201.79760983
201.801754284
201.814187646
201.834909916
202.25349977
201.8183321
202.25349977
202.25349977
519.805709887
519.805709887
519.809854341
519.68552072
519.751831985
519.755976439
519.772554255
519.826432157
519.805709887
202.25349977
519.809854341
519.826432157
202.684522986
519.826432157
519.822287703
519.826432157
519.892743421
202.684522986
201.851487732
201.851487732
201.834909916
202.270077586
202.286655402
201.834909916
201.851487732
202.270077586
202.307377672
202.286655402
202.270077586
202.286655402
201.868065548
202.303233218
202.270077586
201.851487732
202.286655402
202.270077586
202.270077586
202.286655402
202.286655402
202.303233218
202.270077586
201.851487732
201.851487732
201.872210002
201.872210002
201.855632186
201.834909916
201.851487732
201.851487732
201.855632186
201.437042332
201.437042332
201.420464516
201.437042332
201.872210002
201.851487732
201.851487732
201.834909916
201.851487732
201.83905437
201.834909916
201.834909916
201.851487732
201.851487732
201.851487732
201.851487732
201.420464516
201.851487732
201.868065548
201.868065548
201.834909916
201.851487732
201.868065548
201.884643364
201.872210002
201.868065548
201.868065548

Average distance is : 259.171 cm
Average deviation is : 122.718 cm