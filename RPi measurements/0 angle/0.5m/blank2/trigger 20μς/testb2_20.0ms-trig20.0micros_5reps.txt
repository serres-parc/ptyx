
Block: 1
======================================
48.8681317806
48.8352073193
48.8845940113
48.8187450886
48.8352073193

Average distance is : 48.848 cm
Average deviation is : 0.027 cm
Block: 2
======================================
48.8475539923
48.8516695499
48.8516695499
48.8352073193
48.8516695499

Average distance is : 48.848 cm
Average deviation is : 0.007 cm
Block: 3
======================================
48.8722473383
48.8475539923
48.8516695499
48.8516695499
48.8681317806

Average distance is : 48.858 cm
Average deviation is : 0.011 cm
Block: 4
======================================
48.888709569
48.8681317806
48.8516695499
48.8352073193
48.8516695499

Average distance is : 48.859 cm
Average deviation is : 0.020 cm
Block: 5
======================================
48.901056242
48.8845940113
48.8845940113
48.8352073193
49.3167275667

Average distance is : 48.964 cm
Average deviation is : 0.198 cm
Block: 6
======================================
48.8516695499
48.9216340303
48.8845940113
48.8681317806
48.8845940113

Average distance is : 48.882 cm
Average deviation is : 0.026 cm
Block: 7
======================================
48.8681317806
48.8310917616
48.901056242
48.8352073193
48.8352073193

Average distance is : 48.854 cm
Average deviation is : 0.030 cm
Block: 8
======================================
48.8681317806
48.8516695499
48.8845940113
48.8681317806
48.888709569

Average distance is : 48.872 cm
Average deviation is : 0.015 cm
Block: 9
======================================
48.8516695499
48.8516695499
48.8352073193
48.8681317806
48.8681317806

Average distance is : 48.855 cm
Average deviation is : 0.014 cm
Block: 10
======================================
48.8352073193
48.8516695499
48.8845940113
48.8516695499
48.8352073193

Average distance is : 48.852 cm
Average deviation is : 0.020 cm