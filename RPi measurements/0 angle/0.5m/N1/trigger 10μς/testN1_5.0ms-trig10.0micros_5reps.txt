
Block: 1
======================================
50.1093447685
50.0929114342
50.1463197708
50.1093447685
50.1093447685

Average distance is : 50.113 cm
Average deviation is : 0.020 cm
Block: 2
======================================
50.0888031006
50.1093447685
50.0888031006
50.1093447685
50.0929114342

Average distance is : 50.098 cm
Average deviation is : 0.011 cm
Block: 3
======================================
40.0439274788
50.0888031006
50.1093447685
50.1422114372
50.1257781029

Average distance is : 48.102 cm
Average deviation is : 4.505 cm
Block: 4
======================================
50.1257781029
50.1298864365
50.1422114372
50.1093447685
50.1257781029

Average distance is : 50.127 cm
Average deviation is : 0.012 cm
Block: 5
======================================
50.1257781029
50.0929114342
50.1093447685
50.1257781029
50.1257781029

Average distance is : 50.116 cm
Average deviation is : 0.015 cm
Block: 6
======================================
50.1257781029
50.0929114342
50.1257781029
50.0929114342
50.1093447685

Average distance is : 50.109 cm
Average deviation is : 0.016 cm
Block: 7
======================================
49.5259613991
49.4889863968
50.1298864365
50.1257781029
50.0929114342

Average distance is : 49.873 cm
Average deviation is : 0.334 cm
Block: 8
======================================
50.1093447685
50.1093447685
50.1093447685
50.1093447685
50.1463197708

Average distance is : 50.117 cm
Average deviation is : 0.017 cm
Block: 9
======================================
50.1257781029
50.1257781029
50.1257781029
50.1463197708
50.1093447685

Average distance is : 50.127 cm
Average deviation is : 0.013 cm
Block: 10
======================================
50.1422114372
50.0929114342
50.1627531052
50.1257781029
50.1093447685

Average distance is : 50.127 cm
Average deviation is : 0.027 cm