from unittest import TestCase

import Dummy
import Calc

class TestCalc(TestCase):

    dummy = Dummy.Dummy()
    calculation = Calc.Calc()

    def test_find_mean_number(self):
        assert self.dummy.getDataSum() == self.dummy.getDataSumCustom()

    # def test_deviation(self):
    #     value = "%.3f" % float(self.calculation.deviation(self.dummy.getDummyData(),self.dummy.getDataSum()))
    #     print(value)
    #     assert  value== 0.324

    def pop(self):
        assert len(self.dummy.getDummyData()) == 5
        self.dummy.getDummyData().pop()
        assert len(self.dummy.getDummyData()) == 4


