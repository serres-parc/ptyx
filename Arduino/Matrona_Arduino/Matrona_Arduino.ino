#include <stdio.h>

int trigger =9;
int echo = 10;
float TEMPERATURE =25;

void setup() {
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  Serial.begin(9600);
}

void loop() {
  float distance = 0.0;
  distance = findDistance(trigger,echo,TEMPERATURE);
  Serial.print(distance,4);
  delay(3000);
}

float findDistance(int trigger,int echo, float TEMPERATURE){
  float speedOfSound = (331.3 + 0.606 * TEMPERATURE);
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger,LOW);
  float duration = pulseIn(echo,HIGH); // micro-sec
  float distance = duration*(speedOfSound / 10000)/2;
  return distance;
}
