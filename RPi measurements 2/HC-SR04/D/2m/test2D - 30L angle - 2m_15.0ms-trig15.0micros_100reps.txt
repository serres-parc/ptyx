
Block: 1
======================================
200.5753242969513
200.52663111686707
200.60372865200043
201.0257362127304
200.18172109127045
200.6199597120285
200.61184418201447
200.61184418201447
201.05414056777954
201.44368600845337
201.0460250377655
200.14114344120026
200.62807524204254
201.05819833278656
201.49643695354462
200.61590194702148
200.60372865200043
201.05008280277252
201.0257362127304
200.6199597120285
201.05414056777954
201.0460250377655
201.02979397773743
200.5793820619583
200.5996708869934
201.0216784477234
200.5956131219864
200.1289701461792
200.5956131219864
200.60372865200043
200.6199597120285
201.0460250377655
200.14114344120026
201.05414056777954
201.0013896226883
201.46397483348846
200.59155535697937
200.5793820619583
201.01762068271637
201.03790950775146
201.05008280277252
200.55909323692322
200.58749759197235
201.01762068271637
200.98921632766724
200.54286217689514
200.14114344120026
201.03790950775146
200.57126653194427
200.56315100193024
200.11273908615112
200.13708567619324
201.46803259849548
201.01762068271637
201.03385174274445
200.5550354719162
200.59155535697937
201.4720903635025
200.5793820619583
200.14520120620728
200.16549003124237
201.0216784477234
201.48426365852356
201.88598239421844
200.5753242969513
200.5753242969513
200.12491238117218
200.14114344120026
200.12491238117218
200.59155535697937
200.5996708869934
200.57126653194427
200.57126653194427
200.5753242969513
200.59155535697937
201.45585930347443
201.4477437734604
201.03385174274445
201.0460250377655
200.5996708869934
200.54286217689514
201.06225609779358
201.05414056777954
201.0216784477234
200.13302791118622
201.03790950775146
200.5753242969513
200.5956131219864
201.03790950775146
200.5753242969513
200.18983662128448
200.72140383720398
201.4518015384674
201.44368600845337
200.5550354719162
200.56720876693726
200.60372865200043
200.5996708869934
201.0257362127304
200.99327409267426

Average distance is : 200.765 cm
Average deviation is : 0.392 cm
Block: 2
======================================
201.50049471855164
201.01762068271637
201.03385174274445
201.02979397773743
200.52663111686707
200.5550354719162
200.59155535697937
201.46803259849548
200.5793820619583
201.43962824344635
200.54691994190216
200.52663111686707
201.0054473876953
201.0013896226883
200.56315100193024
201.01762068271637
201.88598239421844
201.03790950775146
201.4274549484253
201.01356291770935
200.95675420761108
200.53068888187408
200.5347466468811
200.54691994190216
200.95675420761108
200.98921632766724
200.5793820619583
201.46803259849548
200.56315100193024
200.97704303264618
200.9811007976532
200.60372865200043
202.75840187072754
200.5956131219864
201.05008280277252
200.55909323692322
200.54286217689514
201.88192462921143
200.5793820619583
200.58749759197235
202.33639430999756
200.16143226623535
200.59155535697937
200.55909323692322
201.02979397773743
201.47614812850952
200.5550354719162
201.46803259849548
201.4477437734604
200.55097770690918
201.01762068271637
200.59155535697937
200.5793820619583
201.4477437734604
200.54286217689514
200.58343982696533
200.99733185768127
201.01356291770935
200.57126653194427
200.5753242969513
201.4477437734604
200.55909323692322
201.44368600845337
201.0257362127304
200.12085461616516
201.03385174274445
201.4315127134323
201.05819833278656
201.0257362127304
201.45991706848145
200.58749759197235
200.60372865200043
200.58343982696533
200.60778641700745
200.5793820619583
200.5996708869934
200.55097770690918
200.58343982696533
201.4923791885376
201.46803259849548
201.43962824344635
200.57126653194427
201.01356291770935
201.0216784477234
201.02979397773743
200.58749759197235
200.56720876693726
200.58749759197235
200.57126653194427
200.55909323692322
200.1289701461792
200.58343982696533
201.0216784477234
201.4518015384674
201.05414056777954
201.46397483348846
201.4315127134323
201.48832142353058
201.02979397773743
200.5550354719162

Average distance is : 200.925 cm
Average deviation is : 0.457 cm
Block: 3
======================================
200.52257335186005
201.0013896226883
201.4518015384674
201.0054473876953
200.52257335186005
200.58749759197235
200.50634229183197
201.41933941841125
201.41122388839722
200.5793820619583
202.31204771995544
201.00950515270233
200.56720876693726
200.56315100193024
200.98921632766724
200.56315100193024
200.9811007976532
200.98921632766724
200.9811007976532
201.4477437734604
201.4518015384674
200.97298526763916
200.53880441188812
201.01356291770935
200.97298526763916
200.98515856266022
201.4477437734604
201.4518015384674
201.41528165340424
201.4071661233902
201.43557047843933
200.52663111686707
200.94863867759705
200.99733185768127
201.0257362127304
200.55909323692322
200.58749759197235
201.46397483348846
200.60372865200043
201.43557047843933
201.90221345424652
200.57126653194427
200.54286217689514
201.47614812850952
200.55909323692322
201.01356291770935
201.01356291770935
201.89004015922546
201.0216784477234
200.5550354719162
200.55909323692322
200.58343982696533
201.88598239421844
200.57126653194427
201.89004015922546
201.45991706848145
201.41528165340424
201.86569356918335
201.90221345424652
201.4315127134323
201.47614812850952
200.5996708869934
200.5956131219864
201.4518015384674
200.58749759197235
201.45991706848145
201.0257362127304
201.0013896226883
200.57126653194427
201.46803259849548
200.58343982696533
200.5550354719162
201.45585930347443
201.05414056777954
201.02979397773743
201.48020589351654
201.0216784477234
201.48426365852356
200.5956131219864
200.54691994190216
200.60778641700745
201.4518015384674
201.02979397773743
201.0216784477234
200.99733185768127
200.5996708869934
201.4477437734604
200.57126653194427
201.46803259849548
200.57126653194427
201.50455248355865
200.99733185768127
200.57126653194427
200.60372865200043
201.0216784477234
201.46803259849548
200.6402485370636
201.0460250377655
201.50455248355865
200.60372865200043

Average distance is : 201.045 cm
Average deviation is : 0.437 cm
Block: 4
======================================
200.56720876693726
201.46803259849548
201.92250227928162
200.58343982696533
201.92250227928162
200.98515856266022
201.00950515270233
200.58343982696533
200.14114344120026
200.58343982696533
200.5996708869934
201.0013896226883
200.53068888187408
201.4720903635025
201.0257362127304
200.56720876693726
200.99327409267426
200.55909323692322
200.56720876693726
200.55909323692322
200.54691994190216
200.53880441188812
200.98921632766724
200.53068888187408
200.54286217689514
201.41528165340424
200.55909323692322
201.41933941841125
200.53880441188812
200.54691994190216
200.95675420761108
200.53068888187408
200.514457821846
200.95269644260406
200.52257335186005
201.4477437734604
201.41933941841125
201.02979397773743
202.72188198566437
200.514457821846
200.56720876693726
200.96486973762512
200.94863867759705
200.51851558685303
200.97298526763916
201.41528165340424
200.936465382576
200.9811007976532
200.53068888187408
200.53068888187408
200.51851558685303
201.4315127134323
200.95269644260406
200.54286217689514
200.54691994190216
200.54286217689514
201.41528165340424
200.5996708869934
200.54286217689514
200.99327409267426
200.11679685115814
201.4274549484253
200.5550354719162
201.41528165340424
200.57126653194427
200.55909323692322
201.0216784477234
201.0054473876953
200.5550354719162
200.08027696609497
201.00950515270233
201.0013896226883
200.56720876693726
201.0013896226883
200.99327409267426
201.45585930347443
200.5550354719162
201.43962824344635
200.5347466468811
200.98921632766724
200.52257335186005
200.54691994190216
200.53880441188812
200.08027696609497
200.95675420761108
200.5793820619583
200.99733185768127
200.99733185768127
200.56315100193024
201.01356291770935
200.53068888187408
201.01762068271637
200.56720876693726
201.0257362127304
200.98515856266022
201.41528165340424
201.4274549484253
201.41122388839722
201.41933941841125
200.10056579113007

Average distance is : 200.859 cm
Average deviation is : 0.438 cm
Block: 5
======================================
200.99327409267426
200.58749759197235
201.00950515270233
200.10056579113007
201.0013896226883
201.41528165340424
200.57126653194427
201.39499282836914
201.39499282836914
200.5347466468811
200.56315100193024
201.4315127134323
200.96486973762512
200.96486973762512
202.2836433649063
201.39093506336212
201.82917368412018
200.53068888187408
202.66913104057312
200.53880441188812
200.98515856266022
200.53068888187408
200.53880441188812
200.95269644260406
201.41122388839722
201.41933941841125
200.98515856266022
200.56315100193024
200.50228452682495
200.50634229183197
201.40310835838318
200.514457821846
200.514457821846
200.96486973762512
201.86975133419037
201.84134697914124
200.52257335186005
201.84134697914124
200.54286217689514
200.5347466468811
201.83728921413422
200.514457821846
201.4071661233902
201.39905059337616
200.56720876693726
200.50228452682495
200.50228452682495
200.52663111686707
200.98515856266022
201.41122388839722
201.4315127134323
201.3868772983551
200.52663111686707
201.0013896226883
201.42339718341827
200.55097770690918
200.98515856266022
200.95675420761108
200.51851558685303
201.41528165340424
200.98921632766724
200.10462355613708
200.10056579113007
200.53068888187408
201.4274549484253
200.9608119726181
200.53880441188812
200.52663111686707
200.514457821846
201.42339718341827
201.4274549484253
201.41122388839722
200.53068888187408
200.5550354719162
200.5793820619583
200.55097770690918
201.0257362127304
201.0054473876953
200.510400056839
201.42339718341827
201.00950515270233
201.89004015922546
200.53880441188812
200.50228452682495
200.55097770690918
200.55909323692322
200.09245026111603
200.54691994190216
200.84719455242157
201.03790950775146
200.9608119726181
200.55909323692322
200.54286217689514
200.55097770690918
200.5550354719162
201.42339718341827
200.56720876693726
200.510400056839
200.1086813211441
200.53068888187408

Average distance is : 200.910 cm
Average deviation is : 0.506 cm
Block: 6
======================================
200.50634229183197
200.92429208755493
201.39905059337616
200.50634229183197
200.50634229183197
200.4901112318039
200.51851558685303
201.82511591911316
200.47388017177582
200.5347466468811
200.10462355613708
200.940523147583
199.99100613594055
200.48605346679688
200.0681036710739
201.45585930347443
200.936465382576
200.47793793678284
200.52663111686707
200.9608119726181
200.94863867759705
201.39905059337616
201.44368600845337
200.54286217689514
200.10462355613708
200.5347466468811
200.936465382576
201.43557047843933
201.38281953334808
200.52257335186005
200.53068888187408
200.09245026111603
200.54286217689514
200.51851558685303
200.09245026111603
200.09650802612305
200.53880441188812
200.514457821846
201.82105815410614
200.47793793678284
200.5550354719162
200.53880441188812
201.4071661233902
201.41122388839722
200.10056579113007
200.53880441188812
200.084334731102
200.96892750263214
200.9608119726181
201.40310835838318
200.54691994190216
200.95675420761108
200.54691994190216
200.96486973762512
200.95675420761108
200.56720876693726
200.088392496109
201.38281953334808
200.09650802612305
200.10056579113007
200.54286217689514
200.5347466468811
200.54286217689514
200.936465382576
200.50228452682495
200.52257335186005
200.97704303264618
200.53068888187408
201.00950515270233
200.53068888187408
201.0013896226883
200.53068888187408
200.510400056839
200.510400056839
200.50634229183197
201.84134697914124
200.53068888187408
200.53068888187408
200.53068888187408
200.53880441188812
200.54286217689514
200.5347466468811
200.98515856266022
201.42339718341827
200.50228452682495
201.0054473876953
201.4274549484253
200.55097770690918
200.54691994190216
200.514457821846
200.96486973762512
200.940523147583
200.96892750263214
200.52663111686707
200.93240761756897
200.99733185768127
201.39499282836914
200.91211879253387
200.52663111686707
200.99327409267426

Average distance is : 200.731 cm
Average deviation is : 0.427 cm
Block: 7
======================================
201.8575780391693
201.01356291770935
200.98921632766724
201.01356291770935
201.43962824344635
201.0013896226883
200.98921632766724
201.86163580417633
200.11679685115814
201.43557047843933
200.57126653194427
201.43962824344635
200.98921632766724
200.53068888187408
201.46397483348846
201.43962824344635
201.0013896226883
200.9811007976532
201.40310835838318
201.4071661233902
200.5793820619583
200.57126653194427
200.9811007976532
200.58343982696533
200.5550354719162
200.99327409267426
200.99733185768127
201.88598239421844
202.28770112991333
201.40310835838318
200.56315100193024
200.54691994190216
200.10462355613708
200.09245026111603
200.49822676181793
200.96486973762512
200.96892750263214
201.4274549484253
201.41122388839722
200.58749759197235
200.9608119726181
200.55097770690918
200.5793820619583
200.10056579113007
200.12085461616516
200.53068888187408
200.99733185768127
200.54286217689514
201.41933941841125
201.41122388839722
200.48199570178986
200.95269644260406
200.97298526763916
201.4071661233902
200.51851558685303
200.96486973762512
200.97298526763916
200.09245026111603
200.10056579113007
200.5550354719162
200.97704303264618
200.4901112318039
200.084334731102
200.09245026111603
201.41933941841125
200.510400056839
200.96892750263214
200.07621920108795
200.09650802612305
200.50228452682495
200.0437570810318
200.51851558685303
200.08027696609497
200.510400056839
200.11273908615112
200.97298526763916
200.49822676181793
200.5347466468811
200.50634229183197
200.52663111686707
201.4315127134323
200.54286217689514
202.64884221553802
200.48605346679688
200.514457821846
201.82917368412018
200.510400056839
201.39499282836914
200.09245026111603
200.54286217689514
200.510400056839
200.12491238117218
200.1086813211441
200.10462355613708
200.53068888187408
200.088392496109
200.1086813211441
201.37470400333405
200.96892750263214
200.50634229183197

Average distance is : 200.791 cm
Average deviation is : 0.548 cm
Block: 8
======================================
200.49822676181793
200.56720876693726
200.55909323692322
200.56315100193024
200.56720876693726
200.50228452682495
201.39499282836914
200.940523147583
200.97704303264618
200.4901112318039
200.9608119726181
201.42339718341827
200.50634229183197
200.52663111686707
200.05187261104584
200.10462355613708
200.50228452682495
200.94458091259003
200.98921632766724
200.54286217689514
200.51851558685303
202.26741230487823
201.40310835838318
200.53068888187408
201.4071661233902
200.94863867759705
200.52257335186005
200.084334731102
200.0640459060669
200.1289701461792
200.936465382576
200.10462355613708
200.98921632766724
201.41122388839722
201.4518015384674
200.1086813211441
200.1492589712143
200.08027696609497
200.09650802612305
200.07216143608093
200.97298526763916
200.1086813211441
201.39093506336212
201.44368600845337
200.55097770690918
200.98921632766724
200.52257335186005
200.54286217689514
200.10056579113007
200.09650802612305
200.10462355613708
200.514457821846
200.55909323692322
201.40310835838318
200.52257335186005
201.41933941841125
200.07621920108795
200.54286217689514
200.5347466468811
200.97704303264618
200.54286217689514
201.40310835838318
200.53068888187408
200.55909323692322
200.52663111686707
200.5550354719162
200.50228452682495
200.50634229183197
200.93240761756897
200.10462355613708
201.84134697914124
201.41122388839722
200.1086813211441
200.5347466468811
200.10462355613708
200.96892750263214
200.50228452682495
200.52257335186005
200.93240761756897
200.50228452682495
200.53880441188812
200.52663111686707
200.96486973762512
200.07621920108795
200.48199570178986
200.97704303264618
200.4941689968109
200.53880441188812
200.088392496109
200.09245026111603
201.39905059337616
200.52663111686707
200.96486973762512
200.52257335186005
200.52663111686707
200.52257335186005
200.94863867759705
200.49822676181793
201.4315127134323
200.49822676181793

Average distance is : 200.665 cm
Average deviation is : 0.465 cm
Block: 9
======================================
201.46803259849548
200.510400056839
200.4698224067688
201.0257362127304
200.5347466468811
200.93240761756897
200.12085461616516
200.1086813211441
200.11679685115814
200.50634229183197
200.95675420761108
200.56720876693726
201.41933941841125
200.52257335186005
200.56720876693726
200.56315100193024
200.54691994190216
200.11273908615112
200.53068888187408
200.9608119726181
200.13708567619324
201.41528165340424
200.58343982696533
200.96892750263214
200.09650802612305
200.09650802612305
200.9811007976532
200.12491238117218
200.97704303264618
200.9811007976532
200.9608119726181
201.0013896226883
200.07216143608093
200.53880441188812
200.9811007976532
201.41528165340424
200.52663111686707
200.10056579113007
200.56315100193024
200.53068888187408
200.09650802612305
200.12085461616516
200.08027696609497
200.1086813211441
201.8575780391693
200.9811007976532
200.51851558685303
200.05593037605286
200.514457821846
200.54286217689514
200.50634229183197
200.07216143608093
200.08027696609497
201.40310835838318
200.01941049098969
200.05187261104584
200.49822676181793
200.10056579113007
200.52663111686707
200.08027696609497
200.084334731102
200.96486973762512
200.91211879253387
200.96892750263214
200.9811007976532
200.49822676181793
200.510400056839
200.05998814105988
200.9608119726181
200.52257335186005
200.55097770690918
200.084334731102
200.95675420761108
200.0437570810318
200.08027696609497
200.51851558685303
200.0437570810318
200.0437570810318
200.0681036710739
200.5347466468811
200.51851558685303
200.084334731102
200.0640459060669
200.94458091259003
200.9202343225479
200.92834985256195
200.9608119726181
200.4901112318039
200.0640459060669
200.940523147583
200.52257335186005
200.48199570178986
200.0681036710739
200.0681036710739
200.4941689968109
200.95269644260406
200.54691994190216
200.510400056839
200.0681036710739
200.48199570178986

Average distance is : 200.533 cm
Average deviation is : 0.418 cm
Block: 10
======================================
200.52257335186005
200.95675420761108
200.9161765575409
201.4274549484253
200.50228452682495
200.08027696609497
201.35847294330597
200.5347466468811
201.35441517829895
200.96486973762512
200.94863867759705
200.940523147583
200.95269644260406
200.04781484603882
200.9608119726181
201.3868772983551
200.52663111686707
200.48605346679688
200.92429208755493
201.40310835838318
200.95269644260406
200.49822676181793
200.940523147583
200.50228452682495
200.49822676181793
200.10056579113007
200.95269644260406
201.4315127134323
201.3868772983551
200.9608119726181
200.9161765575409
200.48199570178986
200.5347466468811
201.39499282836914
200.4698224067688
200.09245026111603
200.52257335186005
200.95269644260406
200.936465382576
200.514457821846
200.5347466468811
200.97298526763916
200.07216143608093
200.11273908615112
200.94863867759705
201.37876176834106
202.26335453987122
200.47793793678284
201.37876176834106
200.4901112318039
200.10056579113007
200.07216143608093
200.940523147583
200.52257335186005
200.49822676181793
200.94458091259003
200.95675420761108
200.48199570178986
201.39905059337616
200.940523147583
200.97298526763916
200.98921632766724
200.76198148727417
200.5347466468811
200.12491238117218
200.98921632766724
200.12085461616516
200.0640459060669
200.51851558685303
200.52257335186005
200.62807524204254
200.5550354719162
200.0681036710739
200.58749759197235
200.98921632766724
200.08027696609497
200.53880441188812
201.43557047843933
200.97704303264618
200.11679685115814
200.514457821846
201.40310835838318
201.39093506336212
200.940523147583
200.49822676181793
201.84134697914124
200.52257335186005
200.48199570178986
201.39905059337616
200.50634229183197
201.39093506336212
200.510400056839
201.36658847332
200.5347466468811
200.94863867759705
201.80888485908508
201.41528165340424
200.95269644260406
200.9608119726181
200.514457821846

Average distance is : 200.792 cm
Average deviation is : 0.462 cm